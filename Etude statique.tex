\documentclass{article}
\usepackage[top=2cm, bottom=2cm, left=2cm, right=2cm]{geometry}
\usepackage{color}
\usepackage{amsmath}
\usepackage[utf8]{inputenc}   
\usepackage[T1]{fontenc}
%\usepackage{svg}
\usepackage{graphicx}
\usepackage{eurosym}
\usepackage[francais]{babel}

\definecolor{x0}{rgb}{0.8,0.5,0.2}
\definecolor{y0}{rgb}{0.2,0.8,0.8}
\definecolor{z0}{rgb}{0.5,0.2,0.8}
\definecolor{x1}{rgb}{0.7,0.3,0.3}
\definecolor{y1}{rgb}{0.3,0.7,0.3}
\definecolor{z1}{rgb}{0.3,0.3,0.7}


\newcommand{\SubItem}[1]{
    {\setlength\itemindent{15pt} \item[-] #1}
}

\newcommand{\SubSubItem}[1]{
    {\setlength\itemindent{30pt} \item[-] #1}
}

\begin{document}

\title{Etude d'un enneigeur}
\author{
Benoît \bsc{Dubois}\\
San \bsc{Nguyen}\\
Jean-François \bsc{Ory}\\
Volodia \bsc{Parol-Guarino}\\
Baptiste \bsc{Pétetin}\\
Cécile \bsc{Swichocka-Leonard}
}
\date{\oldstylenums{2019}} 

\maketitle

\part*{Premier modèle}

\section{Introduction}

\subsection{Hypothèses}
Nous avons fait les hypothèses suivantes:
\begin{itemize}
\item la matière est continue ;
\item la matière est homogène ;
\item la matière est isotrope ;
\item la perche et le support sont modélisables par des poutres a une sections constantes ;
\item les deux poutres sont modélisées par des tubes creux ;
\item la compressibilité du fluide est négligé ;
\item les buses sont assimilées à un point ;
\item les buses n'ont pas de masse.
\end{itemize}
Ces considérations faites, nous plaçons notre modèle dans les conditions d'applications de la théorie de Euler-Bernoulli.
\newline
\newline
Nota Bene :
le poids de l'eau circulant dans la conduite qui est à l'intérieur des tubes est négligée dans cette première phase de l'étude.

\subsection{Variables}

On définira pour toute l'étude : 
\begin{itemize}
\item pour la perche (Pe) :
	\SubItem{poids linéique : $P_P$ ;}
	\SubItem{longueur :  $L_P$ ;}
	\SubItem{diamètre extérieur : $D_P$ ;}
	\SubItem{diamètre intérieur : $d_P$ ;}

\item pour le support :
	\SubItem{poids linéique : $P_S$ ;}
	\SubItem{longueur :  $L_S$ ;}
	\SubItem{diamètre extérieur : $D_S$ ;}
	\SubItem{diamètre intérieur : $d_S$ ;}
\item pour la buses
	\SubItem{résultante dû à la poussée de l'eau en sortie des buses sur la perche : $R_B$ ;}
\item{la masse volumique du matériau utilisé: $\rho$ ;}
\item{l'angle entre la perche et le support: $\theta$ ;}
\item{la constante de pesanteur : $g=10$ $m.s^{-2}$.}
\end{itemize}

\subsection{Schéma}

Afin de faciliter la représentation, voici le schéma que nous faisons de la situations.

\begin{center}
\includegraphics[scale=1]{images/modele_schema.pdf}
\end{center}

\subsection{Expressions}

\subsubsection{Poids linéique}
Soit $W_F$ la force linéique correspondante à une force $F$, pour une poutre de longueur $l$, alors $W_F$ s'exprime par:
\newline
$$
W_F = \frac{F}{l}
$$
\newline
Dans le cas plus spécifique du poids :
$$
P=\frac{m g}{l} 
= \frac{\rho V g}{l}
$$
\newline
Où
\begin{itemize}
\item $m$ est la masse de la poutre ;
\item $\rho$ est la masse volumique du matériaux constituant la poutre ;
\item $V$ le volume qu'occupe la poutre ;
\end{itemize}
On peut alors écrire, pour un tube de diamètre extérieur $D$ et intérieur $d$:
$$
V = \frac{\pi(D^2-d^2)}{4}
$$
Et donc que 
$$
P=\rho \pi (D^2-d^2) g
$$
Plus particulièrement :
\begin{itemize}
\item{pour la perche : $P_P=\rho \pi (D_P^2-d_P^2) g$ ;}
\item{pour le support : $P_S=\rho \pi (D_S^2-d_S^2) g$.}
\end{itemize}

\subsubsection{Vent}
Afin de faciliter les calculs nous posons :
\begin{itemize}
\item{$\alpha = \frac{1}{2} \rho_{fluide} C (R_e) {v}_{\infty}^{2} D_P$ pour la perche où $D = D_P$ ;}
\item{$\beta = \frac{1}{2} \rho_{fluide} C (R_e) {v}_{\infty}^{2} D_S$ pour le support où $D = D_S$.}
\end{itemize}
Où:
\begin{itemize}
\item{$\rho_{fluide}$ est la masse volumique du fluide ;}
\item{$C(R_e)$ est le coefficient de traînée (sans dimension) qui dépend :}
\SubItem{de la géométrie du corps ;}
\SubItem{du nombre de Reynolds $R_e = \frac{v_\infty D}{\nu}$ avec :}
\SubSubItem{$v_\infty$ la vitesse du fluide loin du corps (en m/s)}
\SubSubItem{$D$ : dimension caractéristique de l’écoulement (diamètre du cylindre pour un obstacle cylindrique) ;}
\SubSubItem{$\nu$ la viscosité cinématique.}
\end{itemize}

\subsection{Valeurs}
\subsubsection{Alliages}
Les matériaux marqués de "(théorie)" sont utilisés afin de tester les modèles entre RDM6/7 et nos propres calculs. Ceux marqués "(pratique)" sont les options plus sérieusement considérées comme conclusion de cette étude.

\begin{itemize}

\item{Acier inox (théorie) :}
	\SubItem{Module de Young $= 203000$ MPa ;}
	\SubItem{Masse volumique $= 7850$ kg/m3 ;}
	\SubItem{Limite élastique $= 200.00$ MPa ;}
\item{Acier 45 SCD 6 (théorie) :}
	\SubItem{Module de Young $= 220000$ MPa ;}
	\SubItem{Masse volumique $= 7850$ kg/m3 ;}
	\SubItem{Limite élastique $= 1450.00$ MPa ;}
\item{Aluminium (pratique) :}
	\SubItem{Module de Young $= 67500$ MPa ;}
	\SubItem{Masse volumique $= 2700$ kg/m3 ;}
	\SubItem{Limite élastique $= 30.00$ MPa ;}
	\SubItem{Prix par Tonne $= 1640$ \euro $/T$ ;}
\item{Acier inox 1.4404 (316L) (pratique) :}
	\SubItem{Module de Young $= 193000$ MPa ;}
	\SubItem{Masse volumique $= 1992,9$ kg/m3 ;}
	\SubItem{Limite élastique $= 205.00$ MPa ;}
	\SubItem{Prix par Tonne $= 2643$ \euro $/T$.}
	

\end{itemize}

\subsubsection{Dimensions}

Longueurs :
\begin{itemize}
\item{$L_P = 4 \sqrt{5}$ m}
\item{$L_S = 1,5$ m}
\end{itemize}

Diamètres intérieurs :
\begin{itemize}
\item{$d_P=15$ mm ;}
\item{$d_S=15$ mm ;}
\end{itemize}

\subsection{Vent}
On a :
\begin{itemize}
\item{$v_{\infty}=50$ $m.s^{-1}$ ;}
\item{$\nu=15.10^{-6}$ $m^2.s^{-1}$ pour l'air à 20 degrés Celsius et à $p_{atm}$ ;}
\end{itemize}

\subsection{Angles}
On prend $\theta$ tel qu'il modélise le pire des cas possible pour la perche : $\theta= \frac{\pi}{2} - arctan(2)$ rad, correspond au maximum que doit respecter le cahier des charges

\subsection{Buses}
On définit numériquement leurs résultante en un seul point comme suit:
\newline
\begin{center}
$R_B=-350$ N
\end{center}

\subsection{Positions}
On définit la position des points A et B d'après les hypothèses précédentes (longueurs données en mètre) :
\begin{itemize}
\item{$A : (1.5 \overrightarrow{x_0} , 0 \overrightarrow{y_0}, 0 \overrightarrow{z_0});$}
\item{$B : (9.5 \overrightarrow{x_0} , 0 \overrightarrow{y_0}, -4 \overrightarrow{z_0});$}
\end{itemize}


\section{Étude statique}

\subsection{Étude statique de la perche}

$$
    \begin{aligned}
    %Buses sur Perche
    \left\{Buses \rightarrow Perche \right\} &= 
    \left\{
        \begin{array}{ll}
            R_B \cos(\theta) \overrightarrow{z_1} - R_B \sin(\theta) \overrightarrow{x_1} \\
            - L_P R_B \cos(\theta) \overrightarrow{y_1}
        \end{array}
    \right\}
    _A
    \\
    %Pesanteur sur Perche
    \left\{Pesanteur \rightarrow Perche \right\} &= 
    \left\{
        \begin{array}{ll}
            (\sin(\theta) \overrightarrow{z_1} + \cos(\theta) \overrightarrow{x_1})P_P L_P \\
            -P_P \sin(\theta) \frac{{L_P}^{2}}{2} \overrightarrow{y_1}
        \end{array}
    \right\}
    _A
    \\
    %Vent sur Perche
    \left\{Vent \rightarrow Perche \right\} &= 
    \left\{
        \begin{array}{ll}
           	\alpha L_P \overrightarrow{z_1} \\
            - \frac{\alpha {L_P}^{2}}{2} \overrightarrow{y_1}
        \end{array}
    \right\}
    _A
    \\
    %Support sur Perche
    \left\{Support \rightarrow Perche \right\} &= 
    \left\{
        \begin{array}{ll}
            X\overrightarrow{x_1} + Y\overrightarrow{y_1} + Z\overrightarrow{z_1} \\
            L\overrightarrow{x_1} + M\overrightarrow{y_1} + N\overrightarrow{z_1}
        \end{array}
    \right\}
    _A
    \\
    \left\{Support \rightarrow Perche \right\} &= -
    \left\{
        \begin{array}{ll}
            \color{x1}(-R_B \sin(\theta)+P_P L_P \cos(\theta))\overrightarrow{x_1} \color{y1}+ 0\overrightarrow{y_1} \color{z1}+ (R_B \cos(\theta)+P_P L_P \sin(\theta) + \alpha L_P)\overrightarrow{z_1} \\
            \color{x1}0\overrightarrow{x_1} \color{y1} - (L_P R_B \cos(\theta) + (P_P \sin(\theta) + \alpha) \frac{{L_P}^{2}}{2})\overrightarrow{y_1} \color{z1}+ 0\overrightarrow{z_1}
        \end{array}
    \right\}
    _A
    \end{aligned}
$$


\section{Torseurs de cohésions}

\subsection{Torseur de cohésion de la perche}

$$
    \begin{aligned}
    %Buses sur E2
    \left\{Buses \rightarrow Perche \right\} &= 
    \left\{
        \begin{array}{ll}
            R_B \overrightarrow{z_0} \\
            \overrightarrow{0}
        \end{array}
    \right\}
    _B
    = 
    \left\{
        \begin{array}{ll}
            R_B \cos(\theta) \overrightarrow{z_1} - R_B \sin(\theta) \overrightarrow{x_1} \\
            - (L_P-x) R_B \cos(\theta) \overrightarrow{y_1}
        \end{array}
    \right\}
    _G
    \\
    %Pesanteur sur E2
    \left\{Pesanteur \rightarrow E_2 \right\} &= 
    \left\{
        \begin{array}{ll}
            (\sin(\theta) \overrightarrow{z_1} + \cos(\theta) \overrightarrow{x_1}) \int_{x}^{L_P}P_P ds \\
            \int_{x}^{L_P}(s-x)\overrightarrow{x_1} \wedge P_P (\sin(\theta) \overrightarrow{z_1} + \cos(\theta) \overrightarrow{x_1})ds 
        \end{array}
    \right\}
    _G
    = 
    \left\{
        \begin{array}{ll}
            (\sin(\theta) \overrightarrow{z_1} + \cos(\theta) \overrightarrow{x_1})P_P (L_P-x) \\
            - P_P \sin(\theta) \frac{{(L_P-x)}^{2}}{2} \overrightarrow{y_1}
        \end{array}
    \right\}
    _G
    \\
    %Vent sur E2
    \left\{Vent \rightarrow E_2 \right\} &= 
    \left\{
        \begin{array}{ll}
            \int_{x}^{L_P} \alpha ds \overrightarrow{z_1} \\
            \int_{x}^{L_P} (s-x) \overrightarrow{x_1} \wedge \alpha ds \overrightarrow{z_1}
        \end{array}
    \right\}
    _G = 
    \left\{
        \begin{array}{ll}
            \alpha (L_P-x) \overrightarrow{z_1} \\
            - \alpha \frac{{(L_P-x)}^{2}}{2} \overrightarrow{y_1}
        \end{array}
    \right\}
    _G
    \\
    %Torseur de cohésion Perche
    \left\{T_{coh} \right\}_G =
    \left\{\overline{E} \rightarrow E_2 \right\} 
    &= 
    \left\{
        \begin{array}{ll}
            \color{x1}(-R_B\sin(\theta)+P_P(L_P-x)\cos(\theta))\overrightarrow{x_1} \color{y1}+ 0\overrightarrow{y_1} \color{z1}+ (R_B\cos(\theta)+P_P(L_P-x)\sin(\theta)+\alpha(L_P-x))\overrightarrow{z_1} \\
            \color{x1}0\overrightarrow{x_1} \color{y1} + ((x-L_P)R_B\cos(\theta) - (P_P\sin(\theta)+\alpha)\frac{(L_P-x)^{2}}{2})\overrightarrow{y_1} \color{z1}+ 0\overrightarrow{z_1}
        \end{array}
    \right\}
    _G
    \end{aligned}
$$




\subsection{Torseur de cohésion du support}

$$
    \begin{aligned}
    %Pesanteur sur E2'
    \left\{Pesanteur \rightarrow E_2 ' \right\} &= 
    \left\{
        \begin{array}{ll}
             \int_{x'}^{L_S}P_S ds \overrightarrow{x_0} \\
            \int_{x'}^{L_S}P_S ds \overrightarrow{x_0} \wedge \overrightarrow{x_0}
        \end{array}
    \right\}
    _{G'}
    = 
    \left\{
        \begin{array}{ll}
             P_S (L_S-x') \overrightarrow{x_0}\\
             \overrightarrow{0}
        \end{array}
    \right\}
    _{G'}
    \\
    %Vent sur E2'
    \left\{Vent \rightarrow E_2' \right\} &= 
    \left\{
        \begin{array}{ll}
            \int_{x'}^{L_S} \beta ds \overrightarrow{z_0} \\
            \int_{x'}^{L_S} (s-x') \overrightarrow{x_0} \wedge \beta ds \overrightarrow{z_0} \overrightarrow{y_0}
        \end{array}
    \right\}
    _{G'} = 
    \left\{
        \begin{array}{ll}
            \beta (L_S-x') \overrightarrow{z_0} \\
             - \beta \frac{{(L_S-x')}^{2}}{2} \overrightarrow{y_0}
        \end{array}
    \right\}
    _{G'}
    \\
    %============================ début commentaire ================== (iffalse pour masquer et iftrue pour afficher)
    \iffalse
    %Perche sur E2' (dans R1)
    \left\{Perche \rightarrow E_2' \right\} &=
    \left\{
        \begin{array}{ll}
            (-R_B \sin(\theta)+P_P L_P \cos(\theta))\overrightarrow{x_1} + 0\overrightarrow{y_1} + (R_B \cos(\theta)+P_P L_P \sin(\theta) + \alpha L_P)\overrightarrow{z_1} \\
            0\overrightarrow{x_1} + (-L_P R_B \cos(\theta)+(P_P \sin(\theta) + \alpha) \frac{{L_P}^{2}}{2})\overrightarrow{y_1} + 0\overrightarrow{z_1}
        \end{array}
    \right\}
    _{A}
    \end{aligned}
$$

Or

$$
    \begin{aligned}
    \overrightarrow{x_1} &= \cos(\theta)\overrightarrow{x_0} - \sin(\theta)\overrightarrow{z_0}
    \\
    \overrightarrow{y_1} &= \overrightarrow{y_0}
    \\
    \overrightarrow{z_1} &= \sin(\theta)\overrightarrow{x_0} + \cos(\theta)\overrightarrow{z_0}
    \end{aligned}
$$

Donc
$$
    \begin{aligned}
    %Perche sur E2' (dans R0)
    \left\{Perche \rightarrow E_2' \right\} &=
    \left\{
        \begin{array}{ll}
            (P_PL_P\cos^2(\theta) + P_PL_P\sin^2(\theta) + \alpha L_P\sin(\theta))\overrightarrow{x_0} + 0\overrightarrow{y_0} + (R_B \sin^2(\theta) + R_B \cos^2(\theta) + \alpha L_P\cos(\theta))\overrightarrow{z_0} \\
            0\overrightarrow{x_0} + (-L_P R_B \cos(\theta)+(P_P \sin(\theta) + \alpha) \frac{{L_P}^{2}}{2})\overrightarrow{y_0} + 0\overrightarrow{z_0}
        \end{array}
    \right\}
    _{A}
    \\
    &=
    \left\{
        \begin{array}{ll}
            (P_PL_P + \alpha L_P\sin(\theta))\overrightarrow{x_0} + 0\overrightarrow{y_0} + (R_B + \alpha L_P\cos(\theta))\overrightarrow{z_0} \\
            0\overrightarrow{x_0} + (-L_P R_B \cos(\theta)+(P_P \sin(\theta) + \alpha) \frac{{L_P}^{2}}{2})\overrightarrow{y_0} + 0\overrightarrow{z_0}
        \end{array}
    \right\}
    _{A}
    \end{aligned}
$$

On l'exprime au point G' :

$$
    %============================ fin commentaire ==================
    \fi
    \end{aligned}
$$
$$
\begin{aligned}
%Perche sur E2'
    \left\{Perche \rightarrow E_2' \right\} &=
    \left\{
        \begin{array}{ll}
            (P_PL_P + \alpha L_P\sin(\theta))\overrightarrow{x_1} + 0\overrightarrow{y_1} + (R_B + \alpha L_P\cos(\theta))\overrightarrow{z_1} \\
            -(L_P R_B \cos(\theta)+(P_P \sin(\theta) + \alpha) \frac{{L_P}^{2}}{2})\overrightarrow{y_1}
        \end{array}
    \right\}
    _{A}
    \end{aligned}
$$
En l'exprimant dans le repère $R_0$ :
\newline
On pose 
\begin{itemize}
	\item{$a = P_PL_P + \alpha L_P\sin(\theta)$ ; }
	\item{$b = R_B + \alpha L_P\cos(\theta)$ ; }
	\item{$c = -(L_P R_B \cos(\theta)+(P_P \sin(\theta) + \alpha) \frac{{L_P}^{2}}{2}$}
\end{itemize}

$$
\begin{aligned} 
    \left\{Perche \rightarrow E_2' \right\}_{G'} =
    \left\{
        \begin{matrix}
        	a cos(\theta) + b sin(\theta)
        	&
        	0
        	\\
        	0
        	&
        	c - (L_s - x') (-a sin(\theta + b cos(\theta))
        	\\
        	-a sin(\theta) + b cos(\theta)
        	&
        	0
        \end{matrix}
    \right\}
    _{(G', \overrightarrow{x_0}, \overrightarrow{y_0}, \overrightarrow{z_0})}
    \end{aligned}
$$
Ce qui donne le torseur de cohésion suivant :

$$
\begin{aligned} 
    \left\{ T_{coh} \right\}_{G'} =
    \left\{
        \begin{matrix}
        	P_S (L_s -x') + a cos(\theta) + b sin(\theta)
        	&
        	0
        	\\
        	0
        	&
        	c - (L_s - x') (-a sin(\theta + b cos(\theta)) - \beta \frac{(L_s - x')^2}{2})
        	\\
        	-a sin(\theta) + b cos(\theta) + \beta (L_s -x')
        	&
        	0
        \end{matrix}
    \right\}
    _{(G', \overrightarrow{x_0}, \overrightarrow{y_0}, \overrightarrow{z_0})}
    \end{aligned}
$$





\section{Critère de résistance}

\subsection{Contraintes de la perche}

Torseur de cohésion de la forme : 

$$
    \left\{T_{coh} \, Perche\right\} = 
    \left\{
        \begin{array}{ll}
            \color{x1}N\overrightarrow{x_1} \color{y1} + 0\overrightarrow{y_1} \color{z1} + T_z\overrightarrow{z_1} \\
            \color{x1}0\overrightarrow{x_1} \color{y1} + M_{fy}\overrightarrow{y_1} \color{z1} + 0\overrightarrow{z_1}
        \end{array}
    \right\}
$$

\subsubsection{Contraintes de traction/compression}

Soit S la section de la poutre : $S = \pi \frac{D^2 - d^2}{4}$

$$
    \sigma_1 = \frac{N}{S} = \frac{-R_B\sin(\theta) + P_P(L_P-x)\cos(\theta)}{S}
$$
D'après les diagrammes d'effort, la contrainte est maximale en valeur absolue pour $x_{max}=0$.
$$
   \mid  \sigma_1 \mid _{max} = \mid \frac{-R_B\sin(\theta) + P_PL_P \cos(\theta) }{S} \mid
$$

\subsubsection{Contraintes de flexion}

On peut négliger les contraintes tangentielles car la poutre est élancée
\newline

$$
    \sigma_2 = - \frac{M_{fy}}{I(G,\overrightarrow{y_1})} .y = - \frac{(x-L_P)R_B\cos(\theta)-(P_P\sin(\theta)+\alpha)\frac{(L_P-x)^{2}}{2}}{I(G,\overrightarrow{y_1})} .y
$$

D'après les diagrammes d'effort, la contrainte est maximale en valeur absolue pour $x_{max}=0$.
\newline
Pour une poutre circulaire creuse de section circulaire de diamètre extérieur D et de diamètre intérieur d.
\newline
On évalue donc le maximum de contrainte en :

$$
    \begin{aligned}
    \mid y \mid _{max} &= \frac{D}{2}
    \\
    I(G,\overrightarrow{y_1}) &= \frac{\pi (D^4-d^4)}{64}
    \end{aligned}
$$
$$
    \begin{aligned}
    \mid \sigma_2 \mid _{max} &= \mid \frac{(x_{max}-L_P)R_B\cos(\theta)-(P_P\sin(\theta)+\alpha)\frac{(L_P-x_{max})^{2}}{2}}{\frac{\pi (D^4-d^4)}{64}} .\frac{D}{2} \mid
    \\
    \mid \sigma_2 \mid _{max} &= \mid 32D.\frac{(x_{max}-L_P)R_B\cos(\theta)-(P_P\sin(\theta)+\alpha)\frac{(L_P-x_{max})^{2}}{2}}{\pi (D^4-d^4)} \mid
    \end{aligned}
$$

\subsubsection{Critère de résistance}

Coefficient de sécurité : $k = 2$

D'après le théorème de superposition : $\sigma_{tot} = \sigma_1 + \sigma_2$

$$
    \begin{aligned}
    \mid \sigma \mid _{max} &= \mid \sigma_1 + \sigma_2 \mid _{max} \, \leq \, \mid \sigma_1 \mid _{max} + \mid \sigma_2 \mid _{max}
    \\
    \mid \sigma \mid _{max} &\leq \mid \frac{-R_B\sin(\theta) + P_PL_P\cos(\theta)}{\pi \frac{D^2 - d^2}{4}} \mid + \mid - 32D.\frac{(x_{max} -L_P)R_B\cos(\theta)+(P_P\sin(\theta)+\alpha)\frac{(L_P-x_{max} )^{2}}{2}}{\pi (D^4-d^4)} \mid
    \end{aligned}
$$

On cherche à obtenir : $\mid \sigma \mid _{max} \leq \frac{R_{e 0,2}}{k}$


\subsection{Contraintes du support}

Torseur de cohésion de la forme : 

$$
    \left\{T_{coh} \, Support\right\} = 
    \left\{
        \begin{array}{ll}
            \color{x0}N\overrightarrow{x_0} \color{y0} + 0\overrightarrow{y_0} \color{z0} + T_z\overrightarrow{z_0} \\
            \color{x0}0\overrightarrow{x_0} \color{y0} + M_{fy}\overrightarrow{y_0} \color{z0} + 0\overrightarrow{z_0}
        \end{array}
    \right\}
$$

\subsubsection{Contraintes de traction/compression}

Soit S la section de la poutre : $S = \pi \frac{D^2 - d^2}{4}$

$$
    \sigma_1 = \frac{N}{S} = \frac{P_S (L_s -x') + (P_PL_P + \alpha L_P\sin(\theta)) cos(\theta) + (R_B + \alpha L_P\cos(\theta)) sin(\theta)}{S}
$$

%TODO : A corriger
\iffalse

D'après les diagrammes d'effort, la contrainte est maximale en valeur absolue pour $x_{max}=1,5$.

$$
   \mid  \sigma_1 \mid _{max} = \mid \frac{P_SL_S + P_PL_P + \alpha L_P\sin(\theta)}{S} \mid
$$

\fi

\subsubsection{Contraintes de flexion}

On peut négliger les contraintes tangentielles car la poutre est élancée
\newline

$$
    \sigma_2 = - \frac{M_{fy}}{I(G,\overrightarrow{y_0})} .y 
$$
$$
\sigma_2 =  -\frac{-(L_P R_B \cos(\theta)+(P_P \sin(\theta) + \alpha) \frac{L_P^2}{2}) - (L_s - x') (- (P_PL_P + \alpha L_P\sin(\theta)) sin(\theta + (R_B + \alpha L_P\cos(\theta)) cos(\theta)) - \beta \frac{(L_s - x')^2}{2})}
{I(G,\overrightarrow{y_0})} .y
$$

D'après les diagrammes d'effort, la contrainte est maximale en valeur absolue pour $x_{max}=1,5$.
\newline
Pour une poutre circulaire creuse de section circulaire de diamètre extérieur D et de diamètre intérieur d : $d = nD$
\newline
On évalue donc le maximum de contrainte en :

$$
    \begin{aligned}
    \mid y \mid _{max} &= \frac{D}{2}
    \\
    I(G,\overrightarrow{y_0}) &= \frac{\pi (D^4-d^4)}{64}
    \end{aligned}
$$
%TODO à corriger
\iffalse
$$
    \begin{aligned}
    \mid \sigma_2 \mid _{max} &= \mid \frac{\beta \frac{{(L_S-x'_{max})}^{2}}{2} + L_P R_B \cos(\theta) + (P_P \sin(\theta) + \alpha) \frac{{L_P}^{2}}{2} + (L_S-x'_{max})(R_B + \alpha L_P\cos(\theta))}{\frac{\pi (D^4-d^4)}{64}} .\frac{D}{2} \mid
    \\
    \mid \sigma_2 \mid _{max} &= \mid 32D. \frac{\beta \frac{{(L_S-x'_{max})}^{2}}{2} + L_P R_B \cos(\theta) + (P_P \sin(\theta) + \alpha) \frac{{L_P}^{2}}{2} + (L_S-x'_{max})(R_B + \alpha L_P\cos(\theta))}{\pi (D^4-d^4)} \mid
    \end{aligned}
$$

\fi

\subsubsection{Critère de résistance}

Coefficient de sécurité : $k = 2$

D'après le théorème de superposition : $\sigma_{tot} = \sigma_1 + \sigma_2$

%TODO à corriger en fonction des valeurs précédemment corrigées

\iffalse

$$
    \begin{aligned}
    \mid \sigma \mid _{max} &= \mid \sigma_1 + \sigma_2 \mid _{max} \, \leq \, \mid \sigma_1 \mid _{max} + \mid \sigma_2 \mid _{max}
    \\
    \mid \sigma \mid _{max} &\leq \mid\frac{P_SL_S + P_PL_P + \alpha L_P\sin(\theta)}{S} \mid 
    \\
    &+ \mid 32D. \frac{\beta \frac{{(L_S-x'_{max})}^{2}}{2} + L_P R_B \cos(\theta) + (P_P \sin(\theta) + \alpha) \frac{{L_P}^{2}}{2} + (L_S-x'_{max})(R_B + \alpha L_P\cos(\theta))}{\pi (D^4-d^4)} \mid
    \end{aligned}
$$

\fi

On cherche à obtenir : $\mid \sigma \mid _{max} \leq \frac{R_{e 0,2}}{h}$






\section{Critère de flèche}

\subsection{Flèche de la perche}

$$
    \begin{aligned}
    Y_P''(x) &= \frac{M_{fy}}{E.I(G,\overrightarrow{y_1})} = \frac{(x-L_P)R_B\cos(\theta) - (P_P\sin(\theta)+\alpha)\frac{(L_P-x)^{2}}{2}}{E.\frac{\pi (D^4-d^4)}{64}}
    \\
    Y_P'(x) &= \frac{R_B\cos(\theta)(\frac{x^2}{2}-L_Px) - \frac{P_P\sin(\theta) + \alpha}{2}(L_P^2x-L_Px^2+\frac{x^3}{3})}{E.\frac{\pi (D^4-d^4)}{64}} +K_1
    \\
    Y_P(x) &= \frac{R_B\cos(\theta)(\frac{x^3}{6}-\frac{L_Px^2}{2}) - \frac{P_P\sin(\theta) + \alpha}{2}(\frac{L_P^2x^2}{2}-\frac{L_Px^3}{3}+\frac{x^4}{12})}{E.\frac{\pi (D^4-d^4)}{64}} +K_1.x +K'_1
    \\
    Y_P(x) &= 32.\frac{R_B\cos(\theta)(\frac{x^3}{3}-L_Px^2) - (P_P\sin(\theta) + \alpha)(\frac{L_P^2x^2}{2}-\frac{L_Px^3}{3}+\frac{x^4}{12})}{E.\pi (D^4-d^4)} +K_1.x +K'_1
    \end{aligned}
$$


La liaison entre le support et la perche étant un encastrement, on en déduit les conditions initiales suivantes :
\begin{itemize}
\item Y'(0) = 0
\item Y(0) = 0
\end{itemize}

On a donc :
\begin{itemize}
\item $K_1 = 0$
\item $K'_1 = 0$
\end{itemize}

\subsection{Flèche du support}

$$
Y_S''(x') = \frac{M_{fy}}{E.I(G,\overrightarrow{y_0})}
$$

Afin de simplifier l'écriture, on définit le terme constant suivant
\newline
\begin{itemize}
	\item{$a := -(L_P R_B \cos(\theta)+(P_P \sin(\theta) + \alpha) \frac{L_P^2}{2})$ ;}
	\item{$ b := (- (P_PL_P + \alpha L_P\sin(\theta)) sin(\theta) + (R_B + \alpha L_P\cos(\theta)) cos(\theta))$ ;}
\end{itemize}

$$
Y_S''(x') = \frac
{a -b (L_s - x') - \beta \frac{(L_s - x')^2}{2}}
{E.\frac{\pi (D_S^4-d_S^4)}{64}}
$$

$$
Y_S'(x') = \frac
{a x' -b (L_S x' - \frac{x'^2}{2}) - \beta \frac{L_s^2 x' - L_S x'^2 + \frac{x'^3}{3}}{2}}
{E.\frac{\pi (D_S^4-d_S^4)}{64}}
+ K_1
$$

$$
Y_S'(x') = 32. \frac
{2 a x' -b (2 L_S x' - x'^2) - \beta (L_s^2 x' - L_S x'^2 + \frac{x'^3}{3})}
{E.\pi (D_S^4-d_S^4)}
+ K_1
$$

$$
Y_S(x') = 32. \frac
{a x'^2 -b (L_S x'^2 - \frac{x'^3}{3}) - \beta (\frac{L_s^2 x'^2}{2} - \frac{L_S x'^3}{3} + \frac{x'^4}{12})}
{E.\pi (D_S^4-d_S^4)}
+ K_1 x' + K_2
$$


%TODO enlever ces anciennes equations de flèche du support

\iffalse
$$
    \begin{aligned}
    Y_S''(x') &= \frac{M_{fy}}{E.I(G,\overrightarrow{y_0})} = - \frac{\beta \frac{{(L_S-x')}^{2}}{2} + L_P R_B \cos(\theta) + (P_P \sin(\theta) + \alpha) \frac{{L_P}^{2}}{2} + (L_S-x')(R_B + \alpha L_P\cos(\theta))}{E.\frac{\pi (D_S^4-d_S^4)}{64}}
    \\
    Y_S'(x') &= - \frac{\frac{\beta}{2}(L_S^2x' - L_Sx'^2 + \frac{x'^3}{3}) + x' L_P R_B \cos(\theta) + x'(P_P \sin(\theta) + \alpha) \frac{{L_P}^{2}}{2} + (L_Sx'-\frac{x'^2}{2})(R_B + \alpha L_P\cos(\theta))}{E.\frac{\pi (D_S^4-d_S^4)}{64}} + K_2
    \\
    Y_S(x') &= - \frac{\frac{\beta}{2}(\frac{L_S^2x'^2}{2} - \frac{L_Sx'^3}{3} + \frac{x'^4}{12}) + \frac{x'^2}{2} L_P R_B \cos(\theta) + \frac{x'^2}{2}(P_P \sin(\theta) + \alpha) \frac{{L_P}^{2}}{2} + (\frac{L_Sx'^2}{2}-\frac{x'^3}{6})(R_B + \alpha L_P\cos(\theta))}{E.\frac{\pi (D_S^4-d_S^4)}{64}}
    \\
     &+K_2.x' + K'_2
    \\
    Y_S(x') &= -32.\frac{\beta(\frac{L_S^2x'^2}{2} - \frac{L_Sx'^3}{3} + \frac{x'^4}{12}) + x'^2 L_P R_B \cos(\theta) + x'^2(P_P \sin(\theta) + \alpha) \frac{{L_P}^{2}}{2} + (L_Sx'^2-\frac{x'^3}{3})(R_B + \alpha L_P\cos(\theta))}{E.\pi (D_S^4-d_S^4)}
    \\
     &+ K_2.x' + K'_2
     \end{aligned} 
$$
\fi

La liaison entre le support et le sol étant un encastrement, on en déduit les conditions initiales suivantes :
\begin{itemize}
\item Y'(0) = 0
\item Y(0) = 0
\end{itemize}

On a donc :
\begin{itemize}
\item $K_2 = 0$
\item $K'_2 = 0$
\end{itemize}

\subsection{Flèche totale}
On définit de la manière suivante :
\begin{itemize}
\item  $\overrightarrow{u_P}(B)$ : le vecteur de déplacement du point B causé par la flèche de la perche.
\item  $\overrightarrow{u_S}(B)$ : le vecteur de déplacement du point B causé par la flèche du support
\item  $\overrightarrow{u}(B)$ : le vecteur de déplacement du point B causé par les flèches du support et de la perche
\end{itemize} 

Calcul de  $\overrightarrow{u_P}(B)$ :
$$
\begin{aligned}
	\overrightarrow{u_P}(B) &= Y_P(B).\overrightarrow{x_1}
	\\
	&= Y_P(L_P).\overrightarrow{x_1}
	\\
	&= 32.\frac{R_B\cos(\theta)(\frac{L_P^3}{3}-L_P^3) - (P_P\sin(\theta) + \alpha)(\frac{L_P^4}{2}-\frac{L_P^4}{3}+\frac{L_P^4}{12})}{E.\pi (D_P^4-d_P^4)} .\overrightarrow{x_1}
	\\
	&= 32.\frac{-\frac{2}{3}R_B\cos(\theta)L_P^3 - (P_P\sin(\theta) + \alpha)\frac{L_P^4}{4}}{E.\pi (D_P^4-d_P^4)} .\overrightarrow{x_1}
\end{aligned}
$$
Calcul de  $\overrightarrow{u_S}(B)$ :
$$
\overrightarrow{u_S}(B) = Y_S(A).\overrightarrow{x_0} + Y'_S(A).L_P\overrightarrow{x_1}
$$
Calcul de  $\overrightarrow{u}(B)$ :
$$
\begin{aligned}
	\overrightarrow{u}(B) &= \overrightarrow{u_P}(B) + \overrightarrow{u_S}(B)
\end{aligned}
$$


\end{document}
